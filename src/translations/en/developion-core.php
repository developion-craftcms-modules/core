<?php

return [
	"Email" => "Email",
	"Vienna" => "Vienna",
	"Belgrade" => "Belgrade",

	// Button Field
	'Button' => 'Button',
	'Allowed Link Types' => 'Allowed Link Types',
	'Default Text Color' => 'Default Text Color',
	'Default Text Hover Color' => 'Default Text Hover Color',
	'Default Background Color' => 'Default Background Color',
	'Default Background Hover Color' => 'Default Background Hover Color',
	'You must select at least {min, number} of the {attribute}.' => 'You must select at least {min, number} of the {attribute}.',
	"The button text field can't be empty." => "The button text field can't be empty.",
	'The button tag must be selected.' => 'The button tag must be selected.',
	"Entry can't be empty if the link type is Entry." => "Entry can't be empty if the link type is Entry.",
	"Asset can't be empty if the link type is Asset." => "Asset can't be empty if the link type is Asset.",
	"Url can't be empty if the link type is Url." => "Url can't be empty if the link type is Url.",
	"Email can't be empty if the link type is Email." => "Email can't be empty if the link type is Email.",
	"Phone can't be empty if the link type is Phone." => "Phone can't be empty if the link type is Phone.",
];
